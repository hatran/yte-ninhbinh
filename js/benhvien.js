var data = [
 {
   "STT": 1,
   "Name": "Bệnh viện Đa khoa Ninh Bình",
   "address": "Tuệ Tĩnh, Nam Thành, Ninh Bình",
   "Longtitude": 20.243936,
   "Latitude": 105.965464,
   "Rating": 3.98,
   "Number_of_beds": 600,
   "area": "Tỉnh"
 },
 {
   "STT": 2,
   "Name": "Bệnh viện Sản Nhi tỉnh Ninh Bình",
   "address": "Hải Thượng Lãn Ông, Phú Thành, tp. Ninh Bình, Ninh Bình, Việt Nam",
   "Longtitude": 20.250753,
   "Latitude": 105.964298,
   "Rating": 3.98,
   "Number_of_beds": 600,
   "area": "Tỉnh"
 },
 {
   "STT": 3,
   "Name": "Bệnh Viện Mắt Tỉnh Ninh Bình",
   "address": "314 Hải Thượng Lãn Ông, Phú Thành, Ninh Bình, Việt Nam",
   "Longtitude": 20.249979,
   "Latitude": 105.963708,
   "Rating": 3.98,
   "Number_of_beds": 600,
   "area": "Tỉnh"
 },
 {
   "STT": 4,
   "Name": "Bệnh Viện Y Học Cổ Truyền Tỉnh Ninh Bình",
   "address": "QL38B, Đông Thành, Ninh Bình, Việt Nam",
   "Longtitude": 20.267338,
   "Latitude": 105.967403,
   "Rating": 3.98,
   "Number_of_beds": 600,
   "area": "Tỉnh"
 },
 {
   "STT": 5,
   "Name": "Bệnh Viện Lao Phổi Ninh Bình",
   "address": "Phú Thành, Ninh Bình, Việt Nam",
   "Longtitude": 20.250393,
   "Latitude": 105.974303,
   "Rating": 3.98,
   "Number_of_beds": 600,
   "area": "Tỉnh"
 },
 {
   "STT": 6,
   "Name": "Bệnh viện Tâm Thần Tỉnh",
   "address": "Phú Thành, Tp. Ninh Bình, Ninh Bình, Việt Nam",
   "Longtitude": 20.250201,
   "Latitude": 105.961520,
   "Rating": 3.98,
   "Number_of_beds": 600,
   "area": "Tỉnh"
 },
 // {
 //   "STT": 7,
 //   "Name": "Trung tâm Y tế thành phố Vũng Tàu",
 //   "address": "Phường 6, Thành phố Vũng Tàu, Tỉnh Ninh Bình",
 //   "Longtitude": 10.367515,
 //   "Latitude": 107.076392,
 //   "Rating": 3.98,
 //   "Number_of_beds": 600,
 //   "area": "Tỉnh"
 // },
 // {
 //   "STT": 8,
 //   "Name": "Trung tâm Y tế Long Điền",
 //   "address": "Xã Tam Phước, Huyện Long Điền, Tỉnh Ninh Bình",
 //   "Longtitude": 10.464606,
 //   "Latitude": 107.215427,
 //   "Rating": 3.98,
 //   "Number_of_beds": 600,
 //   "area": "Tỉnh"
 // },
 // {
 //   "STT": 9,
 //   "Name": "Trung tâm Y tế Xuyên Mộc",
 //   "address": "Thị Trấn Phước Bửu, Huyện Xuyên Mộc, Tỉnh Ninh Bình",
 //   "Longtitude": 10.5390052,
 //   "Latitude": 107.4108108,
 //   "Rating": 3.98,
 //   "Number_of_beds": 600,
 //   "area": "Tỉnh"
 // },
 // {
 //   "STT": 10,
 //   "Name": "Trung tâm Y tế Tân Thành",
 //   "address": "Thị Trấn Mỹ Xuân, Huyện Tân Thành, Tỉnh Ninh Bình",
 //   "Longtitude": 10.596877,
 //   "Latitude": 107.058933,
 //   "Rating": 3.98,
 //   "Number_of_beds": 600,
 //   "area": "Tỉnh"
 // },
 // {
 //   "STT": 11,
 //   "Name": "Trung tâm Y tế huyện Châu Đức",
 //   "address": "Thị Trấn Ngãi Giao, Huyện Châu Đức, Tỉnh Ninh Bình",
 //   "Longtitude": 10.647814,
 //   "Latitude": 107.240635,
 //   "Rating": 3.98,
 //   "Number_of_beds": 600,
 //   "area": "Tỉnh"
 // },
 // {
 //   "STT": 12,
 //   "Name": "Trung tâm Y tế Quân dân y Côn Đảo",
 //   "address": "Lê Hồng Phong, Huyện Côn Đảo, Tỉnh Ninh Bình",
 //   "Longtitude": 8.7120617,
 //   "Latitude": 106.6011811,
 //   "Rating": 3.98,
 //   "Number_of_beds": 600,
 //   "area": "Tỉnh"
 // },
 // {
 //   "STT": 13,
 //   "Name": "Trung tâm Y tế thành phố Bà Rịa",
 //   "address": "Phường Phước Hiệp,Thành phố Bà Rịa, Tỉnh Ninh Bình",
 //   "Longtitude": 10.498684,
 //   "Latitude": 107.172207,
 //   "Rating": 3.98,
 //   "Number_of_beds": 600,
 //   "area": "Tỉnh"
 // },
 // {
 //   "STT": 14,
 //   "Name": "Trung tâm Y tế huyện Đất Đỏ",
 //   "address": "Tỉnh Lộ 44, Phước Hội, Đất Đỏ, NINH BÌNH, Việt Nam",
 //   "Longtitude": 10.4648746,
 //   "Latitude": 107.2729932,
 //   "Rating": 3.98,
 //   "Number_of_beds": 600,
 //   "area": "Tỉnh"
 // }
];